class_name Player
extends Node2D

var movement_direction := Vector2.ZERO
var tile_position := position

onready var sprite := $Sprite as Sprite

# Only for demonstration.
onready var tilemap := get_node("../TileMap") as TileMap

func _physics_process(delta: float) -> void:
	movement_direction = Vector2(
		(
			int(Input.is_action_just_pressed("ui_right")) -
			int(Input.is_action_just_pressed("ui_left"))
		),
		(
			int(Input.is_action_just_pressed("ui_down")) -
			int(Input.is_action_just_pressed("ui_up"))
		)
	)
	var point_position := tile_position + movement_direction * tilemap.cell_size
	var space_state := get_world_2d().direct_space_state
	var collision := space_state.intersect_point(point_position)
	if collision:
		# Handle the collision
		pass
	else:
		# No collision? No problem!
		tile_position += movement_direction * tilemap.cell_size
	position = lerp(position, tile_position, 0.3)
